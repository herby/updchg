// UMD as of 14 May 2015 from github/umdjs/umd.
(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof exports === 'object') {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {
        // Browser globals (root is window)
        root.returnExports = factory();
    }
}(this, function () {

    function mixin(src, dst) {
        Object.keys(src).forEach(function (k) {
            if (typeof src[k] === "undefined") delete dst[k];
            else dst[k] = src[k];
        });
    }

    function UpdChg(event, methodName) {
        // event name used; defaults to "changed"
        this.event = event || "changed";

        // method name to pass changes into; defaults to "modelChanged"
        this.methodName = methodName || "modelChanged";
    }

    // For jQuery-like events,
    // call this with appropriate wrapper function
    // (jQuery, zepto, ...) to get an instance
    // with proper 'update' and 'observe' to mix in:
    // var updChg = new UpdChg(...).jQuery(jQuery);
    UpdChg.prototype.jQuery = function ($) {
        var event = this.event,
            methodName = this.methodName;

        return {
            // update signalling; using jQuery-like events
            // mix in: `ModelClass.prototype.update = updChg.update;`
            update: function (data) {
                mixin(data, this);
                $(this).trigger(event, data);
            },

            // observing; using jQuery-like events
            // mix in: `ViewClass.prototype.observeModel = updChg.observe;`
            // and make view instance call it once at the startup,
            // or, just make observing externally: `updChg.observe.call(view, model);`
            observe: function (model) {
                var self = this;
                $(model).on(event, function (ev, data) {
                    self[methodName](data, this);
                });
            }
        };
    };

    return UpdChg;
}));
